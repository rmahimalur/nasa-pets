
<!DOCTYPE html>
<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/  dc: http://purl.org/dc/terms/  foaf: http://xmlns.com/foaf/0.1/  og: http://ogp.me/ns#  rdfs: http://www.w3.org/2000/01/rdf-schema#  schema: http://schema.org/  sioc: http://rdfs.org/sioc/ns#  sioct: http://rdfs.org/sioc/types#  skos: http://www.w3.org/2004/02/skos/core#  xsd: http://www.w3.org/2001/XMLSchema# ">
  <head>
    <meta charset="utf-8" />
<link rel="canonical" href="https://usgiad.us/user/login" />
<meta name="robots" content="noindex, nofollow, noarchive, nosnippet, noodp, noydir, noimageindex, notranslate" />
<meta name="Generator" content="Drupal 8 (https://www.drupal.org)" />
<meta name="MobileOptimized" content="width" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<meta http-equiv="x-ua-compatible" content="ie=edge" />
<link rel="shortcut icon" href="/core/misc/favicon.ico" type="image/png" />
<script src="/sites/default/files/google_tag/usgiad_dev/google_tag.script.js?r7nsyd" defer></script>

    <title>Log in | USGIAD</title>
    <link rel="stylesheet" media="all" href="/core/modules/system/css/components/ajax-progress.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/align.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/autocomplete-loading.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/fieldgroup.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/container-inline.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/clearfix.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/details.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/hidden.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/item-list.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/js.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/nowrap.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/position-container.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/progress.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/reset-appearance.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/resize.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/sticky-header.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/system-status-counter.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/system-status-report-counters.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/system-status-report-general-info.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/tabledrag.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/tablesort.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/core/modules/system/css/components/tree-child.module.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/user.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/progress.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/alerts.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/book.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/comments.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/contextual.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/feed-icon.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/field.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/header.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/help.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/icons.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/image-button.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/item-list.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/list-group.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/node-preview.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/page.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/search-form.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/shortcut.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/sidebar.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/site-footer.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/skip-link.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/table.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/tabledrag.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/tableselect.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/tablesort-indicator.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/ui.widget.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/vertical-tabs.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/views.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/ui-dialog.css?r7nsyd" />
<link rel="stylesheet" media="all" href="//stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" />
<link rel="stylesheet" media="all" href="/themes/custom/usgiad_barrio/css/style.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/custom/usgiad_barrio/css/colors.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/form.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/tabs.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/colors/messages/messages-light.css?r7nsyd" />
<link rel="stylesheet" media="all" href="/themes/contrib/bootstrap_barrio/css/components/affix.css?r7nsyd" />
<link rel="stylesheet" media="print" href="/themes/contrib/bootstrap_barrio/css/print.css?r7nsyd" />

    
<!--[if lte IE 8]>
<script src="/sites/default/files/js/js_VtafjXmRvoUgAzqzYTA3Wrjkx9wcWhjP0G4ZnnqRamA.js"></script>
<![endif]-->

  </head>
  <body class="layout-no-sidebars page-user-login path-user">
    <a href="#main-content" class="visually-hidden focusable skip-link">
      Skip to main content
    </a>
    <noscript aria-hidden="true"><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5K74CN3" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas>
    <div id="page-wrapper">
  <div id="page">
    <header id="header" class="header" role="banner" aria-label="Site header">
                      <nav class="navbar navbar-dark bg-primary navbar-expand-lg" id="navbar-main" data-toggle="affix">
                              <a href="/" title="Home" rel="home" class="navbar-brand">
              <img src="/sites/default/files/USAID_Horizontal_White.png" alt="Home" class="img-fluid d-inline-block align-top" />
            USGIAD
    </a>
    <div id="block-cssgeneral" class="block block-block-content block-block-content5f145b74-ac62-4eb5-b130-906d8d03a1f9">
  
    
      <div class="content">
      
            <div class="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden field__item"><style type="text/css">
  .page-view-organizations #main {
    display: none;
  }
  .site-footer {
    background-color: #1f3469;
    padding-left: 32px;
    color: #FFF
  }
  .site-footer a {
    color: #FFF !important;
  }
  section {
    padding: 60px 0px;
  }
  .usgiad-section {
    margin-bottom: 30px;
  }
  .summary {
    font-size: 15px;
  }
  .hero-parent {
    text-align: center;
    align-items: center;
    display: flex;
    justify-content: center;
    width: 100%;
    padding: 60px 30px;
    background-color: #bb2031;
  }
  .hero-child {
    max-width: 1110px;
    display: flex;
    align-items: center;
    color: #fff;
  }
  .home-filters-parent {
    text-align: center;
    align-items: center;
    display: flex;
    justify-content: center;
    width: 100%;
    padding: 60px 30px;
    background-color: #e7edf3;
  }
  a, a:hover, a:visited, a:active {
    color: #003acb;
  }
  .btn-primary {
    background-color: #003acb;
    border: none;
  }
  .btn-primary:hover {
    opacity: 0.8;
  }
  .region-footer-fifth {
    padding: 0px !important;
  }
  #resources-accordion > .card > #heading1, #heading2, #heading3, #heading4, #heading5, #heading6, #heading7, #heading8 {
    display: block;
  }
  .footer-links {
    margin-bottom: 30px;
  }
  .footer-link {
    display: inline-block;
    margin-right: 50px;
    font-size: 15px;
  }
  .site-footer__bottom, .region-footer-fifth {
    border: none !important;
    margin: 0px !important;
  }
  .toast-wrapper {
    position: relative; 
    padding: 15px; 
  }
  .toast.fade {
    opacity: 1;
  }
  #contact-message-feedback-form #edit-preview {
    display: none;
  }
</style></div>
      
    </div>
  </div>


                          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#CollapsingNavbar" aria-controls="CollapsingNavbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
              <div class="collapse navbar-collapse justify-content-end" id="CollapsingNavbar">
                
                	          </div>
                                          </nav>
          </header>
          <div class="highlighted">
        <aside class="container section clearfix" role="complementary">
            <div data-drupal-messages-fallback class="hidden"></div>


        </aside>
      </div>
            <div id="main-wrapper" class="layout-main-wrapper clearfix">
              <div id="main" class="container">
          
          <div class="row row-offcanvas row-offcanvas-left clearfix">
              <main class="main-content col" id="content" role="main">
                <section class="section">
                  <a id="main-content" tabindex="-1"></a>
                    <div id="block-jsjquery" class="block block-block-content block-block-content609fb5d4-fd42-40a4-b4cd-485e562958d6">
  
    
      <div class="content">
      
            <div class="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden field__item"><script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script></div>
      
    </div>
  </div>
<div id="block-componentnavbar" class="block block-block-content block-block-contentfcb041ff-08a6-452e-9ba5-4692c30d9ebd">
  
    
      <div class="content">
      
            <div class="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden field__item"><style type="text/css">#navbar-main {
    background-color: #1f3469 !important;
    color: #fff;
    -webkit-box-shadow: 0px 9px 14px -10px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 9px 14px -10px rgba(0,0,0,0.75);
    box-shadow: 0px 9px 14px -10px rgba(0,0,0,0.75);
  }
  .navbar-dark .navbar-nav .nav-link {
    color: #fff !important;
  }
  .navbar-brand {
    display: flex;
    align-items: center;
    font-weight: 600;
    margin-right: 20px !important;
  }
  .navbar-brand img {
    width: 160px;
    margin-right: 20px;
    border-right: 3px solid #fff;
    padding-right: 20px;
  }
  .navbar .search-block-form .form-group {
    margin-bottom: 0px;
  }
  .navbar .search-block-form .form-control {
    border: none;
    border-radius: 20px;
    outline: none !important;
  }
  .navbar input:focus, .navbar input[type=text]:focus {
    border: 0 !important;
    outline: none !important;
  }
</style></div>
      
    </div>
  </div>
<div id="block-bootstrap-barrio-subtheme-page-title" class="block block-core block-page-title-block">
  
      <h2>Log in</h2>
    
      <div class="content">
      
  <h1 class="title">Log in</h1>


    </div>
  </div>
      <nav class="tabs" role="navigation" aria-label="Tabs">
      
  <h2 class="visually-hidden">Primary tabs</h2>
  <ul class="nav nav-tabs primary"><li class="active nav-item"><a href="/user/login" class="nav-link active nav-link--user-login">Log in</a></li>
<li class="nav-item"><a href="/user/register" class="nav-link nav-link--user-register">Create new account</a></li>
<li class="nav-item"><a href="/user/password" class="nav-link nav-link--user-password">Reset your password</a></li>
</ul>

    </nav>
  <div id="block-bootstrap-barrio-subtheme-idmm-system-main" class="block block-system block-system-main-block">
  
    
      <div class="content">
      
<form class="user-login-form" data-drupal-selector="user-login-form" action="/user/login?destination=/" method="post" id="user-login-form" accept-charset="UTF-8">
  



  <fieldset class="js-form-item js-form-type-textfield form-type-textfield js-form-item-name form-item-name form-group">
          <label for="edit-name" class="js-form-required form-required">Username</label>
                    <input autocorrect="none" autocapitalize="none" spellcheck="false" autofocus="autofocus" data-drupal-selector="edit-name" aria-describedby="edit-name--description" type="text" id="edit-name" name="name" value="" size="60" maxlength="60" class="form-text required form-control" required="required" aria-required="true" />

                          <small id="edit-name--description" class="description text-muted">
        Enter your USGIAD username.
      </small>
      </fieldset>




  <fieldset class="js-form-item js-form-type-password form-type-password js-form-item-pass form-item-pass form-group">
          <label for="edit-pass" class="js-form-required form-required">Password</label>
                    <input data-drupal-selector="edit-pass" aria-describedby="edit-pass--description" type="password" id="edit-pass" name="pass" size="60" maxlength="128" class="form-text required form-control" required="required" aria-required="true" />

                          <small id="edit-pass--description" class="description text-muted">
        Enter the password that accompanies your username.
      </small>
      </fieldset>
<input autocomplete="off" data-drupal-selector="form-miqumu-6vorgix-8pxae6tlfbv8ctyj2daa3wgy3tzc" type="hidden" name="form_build_id" value="form-mIqUmu_6vORGIx-8PXae6tlfbv8Ctyj2daA3Wgy3TZc" class="form-control" />
<input data-drupal-selector="edit-user-login-form" type="hidden" name="form_id" value="user_login_form" class="form-control" />
<div data-drupal-selector="edit-actions" class="form-actions js-form-wrapper form-group" id="edit-actions"><button data-drupal-selector="edit-submit" type="submit" id="edit-submit" name="op" value="Log in" class="button js-form-submit form-submit btn btn-primary">Log in</button>
</div>

</form>

    </div>
  </div>
<div id="block-componentherotitle" class="block block-block-content block-block-content904c4398-6a0a-4aca-9b0b-86aa10cf3d45">
  
    
      <div class="content">
      
            <div class="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden field__item"><p></p>
<script type="text/javascript">
  $(document).ready(function() {
    var title = $("#block-bootstrap-barrio-subtheme-page-title h2").text();
    $("#navbar-main").after( "<div class='hero-parent'><h1 class='hero-child'></h1></div>" );
    $(".hero-child").append(title);
    $("#block-bootstrap-barrio-subtheme-page-title").remove();
  });
</script></p>
</div>
      
    </div>
  </div>
<div id="block-csshidecontact" class="block block-block-content block-block-content1730af49-ed5d-42ee-be64-c2ada1fb0a20">
  
    
      <div class="content">
      
            <div class="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden field__item"><style>
.contact-link {
display: none;
}
</style></div>
      
    </div>
  </div>
<div id="block-jsregistrationtext" class="block block-block-content block-block-contentaddff121-538a-418f-99be-b343a674ef8c">
  
    
      <div class="content">
      
            <div class="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden field__item"><style type="text/css">#registration-summary {
  margin-top: 60px;
}
</style><script>
$(document).ready(function(){
  $("#content").prepend("<div id='registration-summary'><small><em>The U.S. Government International Disaster Response and Assistance Authorities Database serves as an informational resource for active U.S. Government employees. It is only accessible by U.S. Government employees with an active <u>.gov</u> or <u>.mil</u> email address. Any email address that is not an active <u>.gov</u> or <u>.mil</u> address is invalid and will not be granted access.</em></small></div>");
});
</script></div>
      
    </div>
  </div>


                </section>
              </main>
                                  </div>
        </div>
          </div>
        <footer class="site-footer">
              <div class="container">
                                <div class="site-footer__bottom">
                <section class="row region region-footer-fifth">
    <div id="block-disclaimer" class="block block-block-content block-block-contentf41f78f8-764d-4885-880e-4ad9509bc072">
  
    
      <div class="content">
      
            <div class="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden field__item"><div class="footer-links"><a class="footer-link contact-link" href="/contact">Contact</a><a class="footer-link" href="/privacy">Privacy Notice</a><a class="footer-link" href="/508compliance">Section 508 Compliance</a><a class="footer-link" href="https://www.usaid.gov/who-we-are/organization/bureaus/bureau-humanitarian-assistance">USAID/BHA</a></div>
<div>The database is intended to serve as an informational resource for interested U.S. Government agencies and employees. It does not serve as an authoritative or legal accounting of U.S. Government authorities, policies, mandates, or capabilities.</div>
</div>
      
    </div>
  </div>

  </section>

            </div>
                  </div>
          </footer>
  </div>
</div>

  </div>

    
    <script type="application/json" data-drupal-selector="drupal-settings-json">{"path":{"baseUrl":"\/","scriptPath":null,"pathPrefix":"","currentPath":"user\/login","currentPathIsAdmin":false,"isFront":false,"currentLanguage":"en","currentQuery":{"destination":"\/"}},"pluralDelimiter":"\u0003","suppressDeprecationErrors":true,"ajaxTrustedUrl":{"form_action_p_pvdeGsVG5zNF_XLGPTvYSKCf43t8qZYSwcfZl2uzM":true},"user":{"uid":0,"permissionsHash":"ecafd18724e3a8bc5a962aaf4349bf4286ab80f06ce05cf14b97adfc58ddeb04"}}</script>
<script src="/sites/default/files/js/js_LZbzw6GPGOieQI39YrYj0dTZ6k2xKbpdw15cZ4OCgNg.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="//stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="/sites/default/files/js/js_VVvvUUpoxD05nOxorl1vKMeS9ZFscpqbXuTmuBcovkM.js"></script>

  </body>
</html>
