# NASA PETS Sample Code Repository

Sample code Repository for NASA Pets DevSecOps Pipeline

### Key Files and Folders
1. DockerFiles
2. DockerFiles/Jenkinsfile
    * Jenkinsfile in the Dockerfiles folder will build the docker images and publish to the Jfrog artifactory
3. cbci-python-formatter - generate PDF documents that form the basis for SSP document generation
4. Xykon-Code- This directory contains subcontractor developed code that can be mergered with our pipeline with minimal changes while following agreed upon standards
4. ./Jenkinsfile
    * Main Jenkinsfile performs the below tasks
      * Clone the git repository
      * Run the github status checks
      * Performs pull request and branch analysis using SonarQube Scanner
      * Builds the image and scans the artifacts
      * Generate and convert X-ray report to json
      * Create HTML Report
      * Convert Html report to pdf
      * Publish PDF Report to jenkins build
      * Upload the PDF report to S3
5. Use heimdall to view reports in visual format- ![heimdall sample report](Picture1.png)
