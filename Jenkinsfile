import groovy.json.JsonOutput
import java.util.Map
import java.text.SimpleDateFormat

@Library('CISharedLibraries@master')

##### Github Checks Start #####
String compileCheckName = "Code Compile"
String xRayScanCheckName = "X-Ray Scan"
String ghappCredId = "123456-123-123-123-1234567"
##### Github Checks End #####

##### SonarQube Mapping Start #####
Map sqBrConfig=[
      sonarqube:[
        qualityGate:[
          invoke:true,
          timeout:0
        ],
        url:'https://sonarqube.com',
        credentialId:'User_credentialId',
        projectKey:'PrBrAnalysis-Project',
        sources: './src',
        cmdOptions:'-Dsonar.java.binaries="./target/classes"'
      ]
    ]
Map sqPrConfig=[
      sonarqube:[
        qualityGate:[
          invoke:true,
          timeout:0
        ],
        url:'https://sonarqube.com',
        credentialId:'User_credentialId',
        projectKey:'PrBrAnalysis-Project',
        sources: './src',
        cmdOptions:'-Dsonar.java.binaries="./target/classes" -Dsonar.pullrequest.key="$CHANGE_ID" -Dsonar.pullrequest.branch="$CHANGE_BRANCH" -Dsonar.pullrequest.base="$CHANGE_TARGET"'
      ]
    ]
##### SonarQube Mapping End #####

Map gitConfig=[
  git:[
    domain:"github.com",
    credentialId:"User_credentialId",
    org:"T3C",
    repo:"npm-xray-demo",
    buildBranch: "master",
    additionalCmdOptions: "--recursive --recurse-submodules"
    ]
]

Map jfrogGoBuildConfig=[
  jfrog:[
    artifactory:[
			serverName:"rt-server-prod",
    ]
  ]
]

Map triggerXrayBuildScanConfig=[
  jfrog:[
    artifactory:[
			serverName:"rt-server-prod",
			buildName:"T3C-Omnibus-Docker",
			additionalCmdOptions: "--fail=false"
    ]
  ]
]

Map dockerPushToArtifactoryConfig=[
  jfrog:[
    artifactory:[
			serverName:"rt-server-prod",
			buildName:"T3C-Omnibus-Docker",
			targetRepo:"T3C-docker-local",
			credentialId: "T3C-artifactory",
			imageTag:"artifactory.com/T3C-docker-local/npm-xray-demo:${env.BUILD_NUMBER}"
    ]
  ]
]

Map dbtConfig=[
  jfrog:[
    artifactory:[
			serverName:"rt-server-prod",
			buildName:"T3C-omnibus-docker",
			targetRepo:"T3C-docker-local",
			imageTag:"artifactory.com/T3C-docker-local/npm-xray-demo:${env.BUILD_NUMBER}",
			credentialId: "T3C-artifactory"
    ]
  ]
]


pipeline {
    agent {
        kubernetes {
            defaultContainer 'dind'
            yaml """
kind: Pod
metadata:
  name: dind
spec:
  volumes:
  - name: dind-storage
    emptyDir: {}
  containers:
  - name: maven
    image: maven:latest
    imagePullPolicy: Always
    command: ["tail", "-f", "/dev/null"]
    tty: true
  - name: sonarscanner
    image: sonarsource/sonar-scanner-cli:latest
    imagePullPolicy: Always
    command: ["tail", "-f", "/dev/null"]
    tty: true
  - name: dind
    image: artifactory.com/jenkins-core-docker/docker:18.05-dind
    securityContext:
      privileged: true
    volumeMounts:
    - name: dind-storage
      mountPath: /var/lib/docker
  - name: python
    image: artifactory.com/jenkins-core-docker/python:37
    imagePullPolicy: Always
    command: ["tail", "-f", "/dev/null"]
    tty: true
  - name: go
    image: artifactory.com/jenkins-core-docker/go-jfrog-cli:latest
    imagePullPolicy: Always
    command: ["tail", "-f", "/dev/null"]
    tty: true
  - name: heimdalltools
    image: artifactory.com/jenkins-core-docker/heimdall-tools:35
    imagePullPolicy: Always
    command: ["tail", "-f", "/dev/null"]
    tty: true
  - name: pdfutilities
    image: artifactory.com/jenkins-core-docker/pdf-utilities:39
    imagePullPolicy: Always
    command: ["tail", "-f", "/dev/null"]
    tty: true
  nodeSelector:
     Agents: true
"""
        }
    }
    environment
    {
      JFROG_CLI_HOME_DIR = "${env.WORKSPACE}/.jfrog"
      JFROG_CLI_OFFER_CONFIG=false
    }

    options { timestamps() }
    stages
    {

        stage('Git Checkout') {
          steps {
            container('go')
            {
              gitCheckout(gitConfig)
              sh "git config --global submodule.recurse true"
              sh 'git submodule update --init --recursive cbci-python-formatter'
            }
          }
        }

        ##### Github Checks Start #####
        stage("Code Compile"){
            steps{
              script {
                container('go'){
                  try {
                   println "Compling the Code"
                   try {
                      publishCustomGitHubCheckRun([:],ghappCredId,'',compileCheckName, 'npm based Build', '* Succeed for the Base Project', '', 'https://github.T3C.gov/api/v3/')
                   } catch(Exception e) {
                     error("publish Check Run Library failed with error ${e}")
                   }
                  } catch(Exception e) {
                    println "Compliation Failed Will Send the CHecks to GitHub"
                    try {
                      publishCustomGitHubCheckRun([:],ghappCredId,'failure',compileCheckName, '', '', '', 'https://github.T3C.gov/api/v3/')
                    }  catch(Exception err) {
                      error("publish Check Run Library failed with error ${e}")
                   }
                  }
                }
            }
            }
        }

        stage("x-Ray Scan"){
            steps{
              script {
                container('go'){
                  try {
                   error "XRay Stage"
                      publishCustomGitHubCheckRun([:],ghappCredId,'',xRayScanCheckName, '', '', '', 'https://github.T3C.gov/api/v3/')
                  } catch(Exception e) {
                    println "Compliation Failed Will Send the CHecks to GitHub"
                      publishCustomGitHubCheckRun([:],ghappCredId,'action_required',xRayScanCheckName, '', '', '', 'https://github.T3C.gov/api/v3/')
                  }
                }
            }
            }
        }
        ##### Github Checks Start #####

        ##### SonarQube Pull Request and Branch Analysis Start #####
        stage('Performing the Build') {
            steps {
                script {
                    container('maven') {
                            if (env.BRANCH_NAME ==~ "PR-.*") {
                                echo "This is PR analysis"
                                sh "mvn clean package"
                            } else {
                                echo 'This is Branch Analysis'
                                sh "mvn clean package"
                            }
                    }
                }
            }
        }
        stage('SonarScanner Analysis') {
            steps {
                script {
                    container('sonarscanner') {
                            if (env.BRANCH_NAME ==~ "PR-.*") {
                                echo "Performing PR analysis"
                                sonarqubeCodeScan(sqPrConfig)
                            } else {
                                echo 'Performing Branch Analysis'
                                sonarqubeCodeScan(sqBrConfig)
                            }
                    }
                }
            }
        }
        ##### SonarQube Pull Request and Branch Analysis Start #####

        stage("Build and Scan Image")
        {
            steps
            {
                container("dind")
                {
                  sh "apk add  --no-cache --update curl"
                  sh "curl -XGET https://releases.jfrog.io/artifactory/jfrog-cli/v1/1.39.0/jfrog-cli-linux-386/jfrog -L -k -g > jfrog"
                  sh 'mv jfrog /usr/bin'
                  sh "chmod a+x /usr/bin/jfrog"
                  sh "jfrog -v"
                  dockerBuildAndTag(dbtConfig)
                  jfrogCliConfig([:],'', 'T3C-artifactory', '')
                  dockerPushToArtifactory(dockerPushToArtifactoryConfig)
                  collectAndPublishBuildInfo(triggerXrayBuildScanConfig)
                  sh "jfrog rt bs --fail=false T3C-Omnibus-Docker ${BUILD_NUMBER} > T3C-npm-Docker-xray_full.json"

                  //triggerXrayBuildScan(triggerXrayBuildScanConfig)

                }
            }
        }

        stage('Heimdall HDF Task')
        {
            steps
            {
                container('heimdalltools')
                {
                  sh "echo Heimdall converter"
                  //sh 'gem install bigdecimal'
                  sh "heimdall_tools jfrog_xray_mapper -j ./T3C-npm-Docker-xray.json -o ./T3C-npm-Docker-xray.${BUILD_NUMBER}.json"
                }
            }
        }
        stage("Create HTML Report")
        {
            steps
            {
                container("python")
                {
                  sh "echo Convert JSON to HTML"
                  sh "pwd; ls -la; ls -la cbci-python-formatter; python3 cbci-python-formatter/xrayjson2html.py -i T3C-npm-Docker-xray_full.json -o T3C-npm-Docker-xray_full.html"
                }
            }
        }
        stage("Convert Html to pdf")
        {
            steps
            {
                container("pdfutilities")
                {
                    sh 'wkhtmltopdf "--orientation" "Landscape" "--margin-top" "10mm" "--margin-right" "5mm" "--margin-bottom" "5mm" "--margin-left" "5mm" "--encoding" "UTF-8" "--quiet" T3C-npm-Docker-xray_full.html T3C-npm-Docker-xray.pdf'
                }
            }
        }
        stage('Publish PDF Report')
        {
            steps
            {
              container('python')
              {
                sh "pwd; ls -la"
                //archiveArtifacts artifacts: "T3C-npm-Docker-xray.${BUILD_NUMBER}.json", onlyIfSuccessful: false

                publishHTML([
                  allowMissing: false,
                  alwaysLinkToLastBuild: false,
                  includes: '**/*.pdf',
                  keepAll: true,
                  reportDir: '.',
                  reportFiles: 'T3C-npm-Docker-xray.pdf',
                  reportName: 'PDF-Report',
                  reportTitles: ''
                ])

            }
          }
        }

        stage('Copy to S3')
        {
          steps
          {
            container('python')
            {
              withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: 'AWS_ACCESS_KEY_ID', credentialsId: 'CBC-DevSecOps', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']])
              {
                sh "aws s3 cp ./T3C-npm-Docker-xray.${BUILD_NUMBER}.json s3://T3C-jenkins-results/ --sse=AES256"
              }
            }
          }
        }
    }
    post {
      always {
        sh 'echo Done!'
      }
    }
}
