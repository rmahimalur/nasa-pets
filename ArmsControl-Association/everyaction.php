<?php

module_load_include('inc', 'aca_everyaction', 'aca_everyaction.user');
module_load_include('inc', 'aca_everyaction', 'aca_everyaction.client');
module_load_include('inc', 'aca_everyaction', 'aca_everyaction.server');
module_load_include('inc', 'aca_everyaction', 'aca_everyaction.cron');
/**
* Define block / variable constants for the six forms
*/
const everyactionmembership = 'ea-membership-main';
const everyactiondonation = 'ea-donation-main';
const everyactionsubscription = 'ea-subscription-main';
const everyactionagencysubscription = 'ea-agencysubscription-main';
const everyactionmembershipspecial = 'ea-membership-special';
const everyactiondonationspecial = 'ea-donation-special';


if (!function_exists('str_contains')) {
    function str_contains (string $haystack, string $needle)
    {
        return empty($needle) || strpos($haystack, $needle) !== false;
    }
}

/**
* Implements hook_block_info().
*/
function aca_everyaction_block_info() {
  $blocks = array();
  $blocks[everyactionmembership] = array(
    'info' => t('EveryAction Main Membership Form'),
    'cache' => DRUPAL_NO_CACHE
  );
  $blocks[everyactiondonation] = array(
    'info' => t('EveryAction Main Donation Form'),
    'cache' => DRUPAL_NO_CACHE
  );
  $blocks[everyactionsubscription] = array(
    'info' => t('EveryAction Main Subscription Form'),
    'cache' => DRUPAL_NO_CACHE
  );
  $blocks[everyactionagencysubscription] = array(
    'info' => t('EveryAction Main Agency Subscription Form'),
    'cache' => DRUPAL_NO_CACHE
  );
  $blocks[everyactionmembershipspecial] = array(
    'info' => t('EveryAction Special Membership Form'),
    'cache' => DRUPAL_NO_CACHE
  );
  $blocks[everyactiondonationspecial] = array(
    'info' => t('EveryAction Special Donation Form'),
    'cache' => DRUPAL_NO_CACHE
  );
  return $blocks;
}

/**
* Implements hook_block_view().
*/
function aca_everyaction_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case everyactionmembership:
      if (user_access('access content')) {
          $snippet = variable_get(everyactionmembership);
          $snippet = $snippet . variable_get('everyaction_js_snippet');
          $block['content'] = array('#type'=>'markup','#markup'=>$snippet);
      }
      else {
          $block['content'] = t('No content available.');
      }
    break;
    case everyactiondonation:
      if (user_access('access content')) {
          $snippet = variable_get(everyactiondonation);
          $snippet = $snippet . variable_get('everyaction_js_snippet');
          $block['content'] = array('#type'=>'markup','#markup'=>$snippet);
      }
      else {
          $block['content'] = t('No content available.');
      }
    break;
    case everyactionsubscription:
      if (user_access('access content')) {
          $snippet = variable_get(everyactionsubscription);
          $snippet = $snippet . variable_get('everyaction_js_snippet');
          $block['content'] = array('#type'=>'markup','#markup'=>$snippet);
      }
      else {
          $block['content'] = t('No content available.');
      }
    break;
    case everyactionagencysubscription:
      if (user_access('access content')) {
          $snippet = variable_get(everyactionagencysubscription);
          $snippet = $snippet . variable_get('everyaction_js_snippet');
          $block['content'] = array('#type'=>'markup','#markup'=>$snippet);
      }
      else {
          $block['content'] = t('No content available.');
      }
    break;
    case everyactionmembershipspecial:
      if (user_access('access content')) {
          $snippet = variable_get(everyactionmembershipspecial);
          $snippet = $snippet . variable_get('everyaction_js_snippet');
          $block['content'] = array('#type'=>'markup','#markup'=>$snippet);
      }
      else {
          $block['content'] = t('No content available.');
      }
    break;
    case everyactiondonationspecial:
      if (user_access('access content')) {
          $snippet = variable_get(everyactiondonationspecial);
          $snippet = $snippet . variable_get('everyaction_js_snippet');
          $block['content'] = array('#type'=>'markup','#markup'=>$snippet);
      }
      else {
          $block['content'] = t('No content available.');
      }
    break;
  }
  return $block;
}

/**
 * Implements membership page render
 */
function aca_everyaction_membership_page(){
  drupal_set_title("Join or Renew Your Membership");
  $block = block_load('aca_everyaction', everyactionmembership);
  return render(_block_get_renderable_array(_block_render_blocks(array($block))));

}

/**
 * Implements donation page render
 */
function aca_everyaction_donation_page(){
  drupal_set_title("Give Today to Support Arms Control");
  $block = block_load('aca_everyaction', everyactiondonation);
  return render(_block_get_renderable_array(_block_render_blocks(array($block))));

}

/**
 * Implements subscription page render
 */
function aca_everyaction_subscription_page(){
  drupal_set_title("Subscribe to Arms Control Today");
  $block = block_load('aca_everyaction', everyactionsubscription);
  return render(_block_get_renderable_array(_block_render_blocks(array($block))));

}

/**
 * Implements agency subscription page render
 */
function aca_everyaction_agency_page(){
  drupal_set_title("Subscribe to Arms Control Today");
  $block = block_load('aca_everyaction', everyactionagencysubscription);
  return render(_block_get_renderable_array(_block_render_blocks(array($block))));

}

/**
 * Override user/register to forward to /join
 */
function aca_everyaction_menu_alter(&$items) {
 $items['user/register']['page callback'] = 'drupal_goto';
 $items['user/register']['page arguments'] = array('join');
 }

/**
 * Override login form user/register link
 */
function aca_everyaction_form_user_login_block_alter(&$form, &$form_state, $form_id) {
$markup = '<li>'.l(t('Forgot your password?'), 'user/password', array('attributes' => array('title' => t('Request new password via e-mail.')))).'</li>';
  if (variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)) {
    $markup .= '<li>' . l(t('Join'), 'join', array('attributes' => array('title' => t('Create a new user account.'), 'class' => 'register-link'))).'</li>';
  }
  $markup = '<div class="clearfix"><ul>' . $markup . '</ul></div>';
  $form['links']['#markup'] = $markup;

}

/**
 * Implements hook_menu().
 */
function aca_everyaction_menu() {
    $items['agency'] = array(
    'title' => 'Agency Subscription',
    'description' => 'Everyaction agency subscription page',
    'page callback' => 'aca_everyaction_agency_page',
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
  );

    $items['donate'] = array(
    'title' => 'Donate',
    'description' => 'Everyaction donation page',
    'page callback' => 'aca_everyaction_donation_page',
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
  );

    $items['join'] = array(
    'title' => 'Join',
    'description' => 'Everyaction membership/join page',
    'page callback' => 'aca_everyaction_membership_page',
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
  );

    $items['subscribe'] = array(
    'title' => 'Subscribe',
    'description' => 'Everyaction subscription page',
    'page callback' => 'aca_everyaction_subscription_page',
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
  );

$items['admin/config/aca-everyaction'] = [
  'title'            => 'Arms Control - Everyaction Integration',
  'description'      => 'Configure Everyaction settings.',
  'page callback'    => 'drupal_get_form',
  'page arguments' => array('aca_everyaction_admin_settings'),
  'file'             => 'aca_everyaction.admin.inc',
  'access arguments' => array('administer nodes'),
];
return $items;
}


/**
 * hook after login
 */
function aca_everyaction_user_login(&$edit, $account) {
  $existingUser=user_load($account->uid);
  $vanId=aca_everyaction_get_vanid_by_drupalid($account->uid);
  $resp = aca_everyaction_get_contact_activistcode($vanId);

    foreach($resp as $item){
      if(str_contains($item['activistCodeName'],'Digital Access')){
//      if($item['activistCodeName']=='Digital Access'){
          $digitalaccess = true;
          watchdog('aca_everyaction_cron','Found Everyaction activist tag for: '.$account->mail.' vanid: '. $vanId);
      }
    }

    if($digitalaccess){
      watchdog('aca_everyaction_get_contact_membership','Found valid membership for Drupal User' .$account->uid .' email: ' . $account->mail);
      $existingUser=user_load($account->uid);
      aca_everyaction_authorize_user($existingUser);
      $existingUser=user_load($account->uid);
      user_save($existingUser, array());
    }else{
      $existingUser=user_load($account->uid);
      watchdog('aca_everyaction_get_contact_membership', 'Found no membership response for Drupal User' .$account->uid .' email: ' . $account->mail . ' with VanId'.$vanId);
      aca_everyaction_deauthorize_user($existingUser);

    }
}

/**
 * User integration access callback.
 */
function aca_everyaction_user_integration_access($type, $name) {
  if ($type == 'event' || $type == 'condition') {
    return entity_metadata_entity_access('view', 'user');
  }
  // Else return admin access.
  return user_access('administer users');