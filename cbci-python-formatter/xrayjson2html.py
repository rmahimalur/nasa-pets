#!/usr/bin/python3
import sys, getopt, json
from json2html import *

def filterJsonToHtml(inputfile, outputfile):
    with open(inputfile, 'r') as f:
        ojson = json.load(f)
    njson = {}
    njson["Summary"] = ojson["summary"]
    findings = {"High":[], "Medium":[], "Low":[], "Unknown":[]}
    njson["Findings"] = []
    count = 0
    artifact = None
    alert = None
    for alert in ojson["alerts"]:
        for issue in alert["issues"]:
            for artifact in issue["impacted_artifacts"]:
                for file in artifact["infected_files"]:
                    count += 1
                    node={"Component":file["name"]}
                    node["Severity"] = issue["severity"]
                    node['Type'] = issue['type']
                    node['Package Type'] = file['pkg_type']
                    node['Issue Summary'] = issue['summary']
                    node['Description'] = issue['description']
                    findings[node["Severity"]].append(node)
    njson["Findings"] = findings["High"] + findings["Medium"] + findings["Low"] + findings["Unknown"]
    njson["Summary"] = { "Fail_Build":ojson["summary"]["fail_build"],
                        "Total_Alerts":count,
                        "Message": ojson["summary"]["message"],
                        "More_Details_Url":ojson["summary"]["more_details_url"],
                        }
    if artifact:
        njson["Summary"]["Build"] = artifact["display_name"]
    if alert:
        njson["Summary"]["Watch_Name"] = alert["watch_name"]
        njson["Summary"]["Created"] = alert['created'][0:19].replace("T", " ")
    input = json.dumps(njson)
    output = json2html.convert(json=input).replace('<td>High</td>', '<td bgcolor="Crimson">High</td>').replace('<td>Medium</td>', '<td bgcolor="Orange">Medium</td>').replace('<td>Low</td>', '<td bgcolor="Teal">Low</td>').replace('<td>Unknown</td>', '<td bgcolor="Green">Unknown</td>').replace('<table border="1"><tr><th>Summary</th><td>', '<!DOCTYPE html><html><body><font size="2" face="Courier New"><style> table, th, td { border: 1px solid black;} table { width: 100%; border-width: thin; border-collapse: collapse;}</style>').replace('Findings', '<br>')
    outfile = open(outputfile,'w')
    outfile.write(output)
    outfile.close()


if __name__ == '__main__':
    inputfile = None
    outputfile = None
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        print('xrayjson2html.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('xrayjson2html.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    if not inputfile or not outputfile:
        print('xrayjson2html.py -i <inputfile> -o <outputfile>')
    else:
        filterJsonToHtml(inputfile, outputfile)
